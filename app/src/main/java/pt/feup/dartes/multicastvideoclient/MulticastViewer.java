package pt.feup.dartes.multicastvideoclient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class MulticastViewer extends ActionBarActivity {

    SectionsPagerAdapter mSectionsPagerAdapter;

    static ViewPager mViewPager;


    static String EXE_NAME="main_client";
    static String CACHE_BASEDIR ="/data/data/pt.feup.dartes.multicastvideoclient/cache/";
    static String FILES_BASEDIR ="/data/data/pt.feup.dartes.multicastvideoclient/files/";
    static String PATH="rtp://127.0.0.1:30000";

    static JSONObject coding_configuration_json = null;
    static JSONObject network_configuration_json = null;

    static String main_pid =null;


    private static void copyFileAssets(String assetPath, String localPath, Context context) {
        try {
            InputStream in = context.getAssets().open(assetPath);
            FileOutputStream out = new FileOutputStream(localPath);
            int read;
            byte[] buffer = new byte[4096];
            while ((read = in.read(buffer)) > 0) {
                out.write(buffer, 0, read);
            }
            out.close();
            in.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void copyFile(String fromPath, String localPath) {
        try {
            InputStream in = new FileInputStream(fromPath);
            FileOutputStream out = new FileOutputStream(localPath);
            int read;
            byte[] buffer = new byte[4096];
            while ((read = in.read(buffer)) > 0) {
                out.write(buffer, 0, read);
            }
            out.close();
            in.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }



    public String readFromAsset(String file) {
        String json = null;
        try {

            InputStream is = getAssets().open(file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private static void writeToFile(String data, String file, Context x) {
    try {
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(x.openFileOutput(file, Context.MODE_PRIVATE));
        outputStreamWriter.write(data);
        outputStreamWriter.close();
    }
    catch (IOException e) {
        Log.e("Exception", "File write failed: " + e.toString());
    }
}


    private String readFromFile(String file) throws FileNotFoundException{

        String ret = "";
        try {
            InputStream inputStream = openFileInput(file);

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    private static void killall(){
        //try { Runtime.getRuntime().exec(new String[] {"su", "-c", "killall "+EXE_NAME}); }


        if (main_pid!=null) {
            try { Runtime.getRuntime().exec(new String[]{"kill", main_pid}); }
            catch (IOException e) { e.printStackTrace(); }
        }

        try { Runtime.getRuntime().exec(new String[] {"killall",EXE_NAME}); }
        catch (IOException e) { e.printStackTrace(); }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multicast_viewer);


        /*Just in case*/
        killall();
        try { Thread.sleep(1000); } catch (InterruptedException e) { }

        //writeToFile("Hi there", "testfile.txt");
        String coding_configuration_string=null;
        try {
            coding_configuration_string=readFromFile("coding_configuration.json");
        } catch (FileNotFoundException e) {
            coding_configuration_string=readFromAsset("coding_configuration.json"); }

        String network_configuration_string=null;
        try { network_configuration_string=readFromFile("network_configuration.json");
        } catch (FileNotFoundException e) { network_configuration_string=readFromAsset("network_configuration.json"); }

        coding_configuration_json = null;
        network_configuration_json = null;
        try {
            coding_configuration_json = new JSONObject(coding_configuration_string);
            network_configuration_json = new JSONObject(network_configuration_string);
            //Log.i("1", network_configuration_json.getString("stream_address") );

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        copyFileAssets(EXE_NAME, CACHE_BASEDIR + EXE_NAME, this);
        //copyFileAssets("coding_configuration.json", CACHE_BASEDIR + "coding_configuration.json", this);
        //copyFileAssets("network_configuration.json", CACHE_BASEDIR + "network_configuration.json", this);

        (new File(CACHE_BASEDIR +EXE_NAME)).setExecutable(true);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        killall();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_multicast_viewer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            String coding_configuration_string=readFromAsset("coding_configuration.json");
            String network_configuration_string=readFromAsset("network_configuration.json");

            coding_configuration_json = null;
            network_configuration_json = null;
            try {
                coding_configuration_json = new JSONObject(coding_configuration_string);
                network_configuration_json = new JSONObject(network_configuration_string);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //write to file
            if (network_configuration_json!=null)
                writeToFile(network_configuration_json.toString(), "network_configuration.json", this);
            if (coding_configuration_json!=null)
                writeToFile(coding_configuration_json.toString(), "coding_configuration.json", this);


            finish();
            //Fragment f=mSectionsPagerAdapter.getItem(0);
            //getSupportFragmentManager().beginTransaction().detach(f).attach(f).commit();

            return true;
        }

        if (id == R.id.action_about){
            Toast.makeText(this, "CodeStream Project", Toast.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0: return getString(R.string.title_section1).toUpperCase(l);
                case 1: return getString(R.string.title_section2).toUpperCase(l);
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            int sectionNumber = this.getArguments().getInt(ARG_SECTION_NUMBER);
            View rootView=null;
            if (sectionNumber == 1) {
                rootView = inflater.inflate(R.layout.fragment_multicast_viewer, container, false);

                Switch sw = (Switch)rootView.findViewById(R.id.switch1);
                final EditText txt_multicast_ip = (EditText)rootView.findViewById(R.id.editmulticastip);
                final EditText txt_multicast_port = (EditText)rootView.findViewById(R.id.editmulticastport);

                final Button btn_videoEmbedd = (Button)rootView.findViewById(R.id.buttonEmbedd);
                final Button btn_videoOutSource = (Button)rootView.findViewById(R.id.btnoutsource);

                final EditText txt_multicast_clientID = (EditText)rootView.findViewById(R.id.clientid);

                btn_videoEmbedd.setEnabled(false);
                btn_videoOutSource.setEnabled(false);

                if (network_configuration_json!=null){
                    try {
                        txt_multicast_ip.setText(network_configuration_json.getString("stream_address"));
                        txt_multicast_port.setText(String.valueOf(network_configuration_json.getInt("stream_port")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            txt_multicast_ip.setEnabled(false);
                            txt_multicast_port.setEnabled(false);
                            txt_multicast_clientID.setEnabled(false);

                            btn_videoEmbedd.setEnabled(true);
                            btn_videoOutSource.setEnabled(true);

                            /* Save the edited configurations */
                            if (network_configuration_json!=null){
                                try {
                                    network_configuration_json.put("stream_address",txt_multicast_ip.getText());
                                    network_configuration_json.put("stream_port", Integer.valueOf(txt_multicast_port.getText().toString()));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }


                            /* Save the current configurations */
                            if (network_configuration_json!=null)
                                writeToFile(network_configuration_json.toString(), "network_configuration.json", buttonView.getContext());
                            if (coding_configuration_json!=null)
                                writeToFile(coding_configuration_json.toString(), "coding_configuration.json", buttonView.getContext());

                            String clientID = null;
                            try{
                                clientID = String.valueOf(Integer.valueOf(txt_multicast_clientID.getText().toString()));
                            }catch ( NumberFormatException e){
                                clientID = "5";
                                txt_multicast_clientID.setText("5");
                            }

                            final String finalClientID = clientID;
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    //try { p = Runtime.getRuntime().exec(new String[] {"su", "-c", command}); }
                                    //catch (IOException e) { e.printStackTrace(); }

                                    final String command= CACHE_BASEDIR + EXE_NAME+ " -c " + FILES_BASEDIR + "coding_configuration.json -s " + FILES_BASEDIR + "network_configuration.json  -i "+ finalClientID;

                                    Log.i("1", command);
                                    try {
                                        //ProcessBuilder pb = new ProcessBuilder("su","-c",command);
                                        ProcessBuilder pb = new ProcessBuilder(CACHE_BASEDIR + EXE_NAME, "-c", FILES_BASEDIR + "coding_configuration.json", "-s", FILES_BASEDIR + "network_configuration.json", "-i", finalClientID);
                                        pb.redirectErrorStream(true);
                                        Process p = pb.start();
                                        //p.get

                                        Field f = p.getClass().getDeclaredField("pid");
                                        f.setAccessible(true);
                                        main_pid = String.valueOf(f.getInt(p));
                                        //Log.i("1","--------------------------"+f.getInt(p));


                                        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                        String line;
                                        while ((line = reader.readLine()) != null) {
                                            //System.out.println("tasklist: " + line);
                                            ;
                                        }
                                        p.waitFor();
                                    }
                                    catch (IOException e) { e.printStackTrace(); }
                                    catch (InterruptedException e) { e.printStackTrace(); }
                                    catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                    } catch (NoSuchFieldException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();


                        }else{
                            txt_multicast_ip.setEnabled(true);
                            txt_multicast_port.setEnabled(true);
                            txt_multicast_clientID.setEnabled(true);

                            btn_videoEmbedd.setEnabled(false);
                            btn_videoOutSource.setEnabled(false);

                            killall();

                        }
                    }
                });

                btn_videoOutSource.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        /* Stop vitamio if running */
                        //mVideoView.stopPlayback();

                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(PATH));
                        ////intent.setDataAndType(Uri.parse(newVideoPath), "video/mp4");
                        startActivity(intent);
                    }
                });

                btn_videoEmbedd.setOnClickListener(new View.OnClickListener() {
                    @Override public void onClick(View v) {
                        mViewPager.setCurrentItem(1);
                    }
                });
                
            }else{

                rootView = new LinearLayout(inflater.getContext());
                ((LinearLayout)rootView).setOrientation(LinearLayout.VERTICAL);
                ((LinearLayout)rootView).setGravity(Gravity.CENTER_HORIZONTAL);


                //if (!io.vov.vitamio.LibsChecker.checkVitamioLibs(inflater.getContext()))
                //    return;

                Log.i("1", "Adding Video....");


            }
            return rootView;
        }
    }

}
